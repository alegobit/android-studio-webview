package com.alegodroid.webview;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.MailTo;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    WebView myWebView;
    private SwipeRefreshLayout swipeLayout;
    String myWebSite = "http://www.alegodroid.com";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //int myColor = Color.parseColor("#197cd9");
        swipeLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_container);
        //swipeLayout.setProgressBackgroundColorSchemeColor(myColor);
        swipeLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        swipeLayout.setRefreshing(false);
                    }
                }, 5000);
            }
        });

        myWebView = (WebView) findViewById(R.id.webView);

        myWebView.setWebChromeClient(new WebChromeClient() {
            @Override
            public void onReceivedTitle(WebView view, String title) {
                getWindow().setTitle(title); //Set Activity tile to page title.
            }
        });

        myWebView.setWebViewClient(new WebViewClient() {
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                findViewById(R.id.progress1).setVisibility(View.VISIBLE);
            }
            @Override
            public void onPageFinished(WebView view, String url) {
                findViewById(R.id.progress1).setVisibility(View.GONE);
            }
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                try{
                    System.out.println("url called:::" + url);
                    if (url.startsWith("tel:")) {
                        Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse(url));
                        startActivity(intent);
                    }  else if (url.startsWith("https:")) {
                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                        startActivity(intent);
                    }
                    else {
                        view.loadUrl(url);
                        return false;
                    }
                }catch(Exception e){
                    e.printStackTrace();
                }
                return true;
            }
        });

        myWebView.getSettings().setJavaScriptEnabled(true);
        myWebView.getSettings().setGeolocationEnabled(true);
        myWebView.getSettings().setSupportMultipleWindows(true); // This forces ChromeClient enabled.

        /* startof enabling local storage */
        myWebView.getSettings().setDomStorageEnabled(true);
        myWebView.getSettings().setDatabaseEnabled(true);
        if(Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT) {
            myWebView.getSettings().setDatabasePath(
                    "/data/data/" + myWebView.getContext().getPackageName() + "/databases/"
            );
        }
        /* endof enabling local storage */

        if (DetectConnection.checkInternetConnection(this)) {
            myWebView.loadUrl(myWebSite);
        }else {
            myWebView.loadUrl("file:///android_asset/www/index.htm");
        }

        /* not showing the action bar */
        getSupportActionBar().hide();

    }

}


