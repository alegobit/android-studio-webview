# README #

Create New Android Project, follow the wizards, once completed just copy the [app] folder, duplicate existing entry

## Internet available Load External Website otherwise Load Internal HTML Assets (ver2) ##
\app\src\main\java\com\alegodroid\webview\
**MainActivity.java**
```
package com.alegodroid.webview;
...
public class MainActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        ...
        if(DetectConnection.checkInternetConnection(this)){
            myWebView.loadUrl("http://your.site.domain");
        } else {
            myWebView.loadUrl("file:///android_asset/www/index.htm");
        }
        ...
    }
}
```
Dependency class: \app\src\main\java\com\alegodroid\webview\
**DetectConnection.java**
Dependency permission: \app\src\main\
**AndroidManifest.xml**
```
    <uses-permission android:name="android.permission.INTERNET" />
    <uses-permission android:name="android.permission.ACCESS_NETWORK_STATE" />
```

## Load External Website (ver1) ##
\app\src\main\java\com\alegodroid\webview\
**MainActivity.java**
```
package com.alegodroid.webview;
...
public class MainActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        ...
        myWebView.loadUrl("http://your.site.domain");
    }
}
```

## Load Internal HTML Assets (ver1) ##
First copy all your HTML Assets into \app\src\main\assets\ folder,
then do the MainActivity class as below
\app\src\main\java\com\alegodroid\webview\
**MainActivity.java**
```
package com.alegodroid.webview;
...
public class MainActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        ...
        myWebView.loadUrl("file:///android_asset/www/yourindexfile.html");
    }
}
```

## Open https urls on a browser (outside webview) ##
\app\src\main\java\com\alegodroid\webview\
**MainActivity.java**
```
package com.alegodroid.webview;
...
public class MainActivity extends AppCompatActivity {
	...
    myWebView.setWebViewClient(new WebViewClient() {
    	...
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
        	try{
            	System.out.println("url called:::" + url);
                if (url.startsWith("tel:")) {
                	Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse(url));
                    startActivity(intent);
                } else if (url.startsWith("https:")) {
                	Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                    startActivity(intent);
                } else {
                	view.loadUrl(url);
                    return false;
                }
			}catch(Exception e){
            	e.printStackTrace();
			}
            return true;
		}
	});
    ...
}
```

## Enable Javascript ##
\app\src\main\java\com\alegodroid\webview\
**MainActivity.java**
`myWebView.getSettings().setJavaScriptEnabled(true);`

## Loading Progress ##
\app\src\main\res\layout\
**activity_main.xml**
```
    <ProgressBar
        android:id="@+id/progress1"
        android:layout_centerHorizontal="true"
        android:layout_centerVertical="true"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content" />
```
\app\src\main\java\com\alegodroid\webview\
**MainActivity.java**
```
package com.alegodroid.webview;
...
public class MainActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        ...

        myWebView.setWebViewClient(new WebViewClient() {
            public void onPageStarted(WebView view, String url, Bitmap favicon) {

                super.onPageStarted(view, url, favicon);
                findViewById(R.id.progress1).setVisibility(View.VISIBLE);
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                findViewById(R.id.progress1).setVisibility(View.GONE);
            }

            ...
            }
        });
    }
}
```

## Pull to Refresh Implementation on WebView ##
\app\src\main\res\layout\
**activity_main.xml**
```
<RelativeLayout ... >
    
    <FrameLayout
        android:id="@+id/frameLayout"
        android:layout_width="match_parent"
        android:layout_height="match_parent"
        android:fillViewport="true"
        android:background="#ffffff">
        <android.support.v4.widget.SwipeRefreshLayout
            android:id="@+id/swipe_container"
            android:layout_width="match_parent"
            android:layout_height="match_parent"
            android:fillViewport="true">
            <WebView
                android:layout_width="fill_parent"
                android:layout_height="fill_parent"
                android:id="@+id/webView"
                android:layout_alignParentStart="true"
                android:layout_alignParentLeft="true"
                android:layout_alignParentBottom="true" />
        </android.support.v4.widget.SwipeRefreshLayout>
    </FrameLayout>

    <ProgressBar ... />
</RelativeLayout >
```
\app\src\main\java\com\alegodroid\webview\
**MainActivity.java**
```
package com.alegodroid.webview;
...
public class MainActivity extends AppCompatActivity {
	...
    private SwipeRefreshLayout swipeLayout;
	...
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        ...

        swipeLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_container);
		swipeLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
			@Override
			public void onRefresh() {
				new Handler().postDelayed(new Runnable() {
					@Override
					public void run() {
						swipeLayout.setRefreshing(false);
					}
 				}, 5000);
 			}
 		});

        ...
    }
}
```

- - -

## Others ##
### No Action Bar ###
\app\src\main\res\values\
**styles.xml**
`<style name="AppTheme" parent="Theme.AppCompat.NoActionBar"></style>`

### Force Landscape Mode ###
\app\src\main\
**AndroidManifest.xml**
```
<activity android:name=".MainActivity"
            android:screenOrientation="landscape"></activity>
```